//
//  RigKeeper.swift
//  Riginator
//
//  Created by Buck Tower on 5/19/15.
//  Copyright (c) 2015 Buck Tower. All rights reserved.
//

import Foundation

class RigKeeper {
    var genre = ""
    var subgenre = ""
    var model = ""
}