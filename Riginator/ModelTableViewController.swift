//
//  ModelTableViewController.swift
//  Riginator
//
//  Created by Buck Tower on 5/15/15.
//  Copyright (c) 2015 Buck Tower. All rights reserved.
//

import UIKit

class ModelTableViewController: UITableViewController {

    // Temp Rig
    var theRig:RigKeeper? = RigKeeper()
    @IBOutlet var theTable: UITableView!
    
    // PList
    var dictionaryFromFile:Dictionary<String,AnyObject>?
    var modelsList:Dictionary<String,Dictionary<String,String>> = [:]
    var currentModelsDict:Dictionary<String,String> = [:]
    var keyNames:Array<String> = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        
        
        let fileLocation = NSBundle.mainBundle().pathForResource("CustomizationReference", ofType: "plist")
        dictionaryFromFile = (NSDictionary(contentsOfFile: fileLocation!) as? Dictionary<String,AnyObject>)!
        
        if let dict = dictionaryFromFile
        {
            modelsList = (dict["Models"]) as! Dictionary<String,Dictionary<String,String>>
        }
        
        currentModelsDict = modelsList[theRig!.subgenre]!
    }
    
    func setRig(rig: RigKeeper)
    {
        theRig = rig
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
            return currentModelsDict.count
    }

    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("reuseIdentifier", forIndexPath: indexPath) as! UITableViewCell
        
            let keySet = currentModelsDict.keys
            for key in keySet
            {
                println(key)
                keyNames.append(key)
                
            }
            println("Value for row \(indexPath.row): \(keyNames[indexPath.row])")
            
            cell.textLabel!.text = keyNames[indexPath.row]
            //cell.detailTextLabel?.text = keyNames


        
        // Configure the cell...

        return cell
    }
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the item to be re-orderable.
        return true
    }
    */

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
        if let path = tableView.indexPathForSelectedRow()
        {
            if let currentModelsDict:Dictionary<String,String> = modelsList[theRig!.subgenre]
            {
                theRig?.model = keyNames[path.row]
                println("User picked \(theRig?.model) as their model.")
            }
        }
        
        if let nextVC = segue.destinationViewController as? ResultViewController
        {
            nextVC.setRig(theRig!)
        }
    }
    

}
