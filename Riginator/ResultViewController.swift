//
//  ResultViewController.swift
//  Riginator
//
//  Created by Buck Tower on 5/15/15.
//  Copyright (c) 2015 Buck Tower. All rights reserved.
//

import UIKit

class ResultViewController: UIViewController {

    @IBOutlet weak var modelLabel: UILabel!
    @IBOutlet weak var genreLabel: UILabel!
    
    // Temp Rig
    var theRig:RigKeeper? = RigKeeper()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        modelLabel.text = theRig?.model
        genreLabel.text = theRig?.subgenre
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setRig(rig: RigKeeper)
    {
        theRig = rig
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
