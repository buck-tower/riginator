//
//  Rig.swift
//  Riginator
//
//  Created by Buck Tower on 5/18/15.
//  Copyright (c) 2015 Buck Tower. All rights reserved.
//

import Foundation
import CoreData

class Rig: NSManagedObject {
    
    @NSManaged var genre: String
    @NSManaged var subgenre: String
    @NSManaged var model: String
    
    @NSManaged var userTitle: String
    
    override var description:String {
        if(userTitle != ""){
            return "\(userTitle)"
        }
        else {
            return "\(subgenre) + \(model)"
        }
    }
    
}